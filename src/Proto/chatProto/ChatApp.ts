// Original file: src/Proto/ChatProto.proto

import type * as grpc from '@grpc/grpc-js'
import type { MethodDefinition } from '@grpc/proto-loader'
import type { allChatRequest as _chatProto_allChatRequest, allChatRequest__Output as _chatProto_allChatRequest__Output } from '../chatProto/allChatRequest';
import type { allChatResponse as _chatProto_allChatResponse, allChatResponse__Output as _chatProto_allChatResponse__Output } from '../chatProto/allChatResponse';

export interface ChatAppClient extends grpc.Client {
  allChats(argument: _chatProto_allChatRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_chatProto_allChatResponse__Output>): grpc.ClientUnaryCall;
  allChats(argument: _chatProto_allChatRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_chatProto_allChatResponse__Output>): grpc.ClientUnaryCall;
  allChats(argument: _chatProto_allChatRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_chatProto_allChatResponse__Output>): grpc.ClientUnaryCall;
  allChats(argument: _chatProto_allChatRequest, callback: grpc.requestCallback<_chatProto_allChatResponse__Output>): grpc.ClientUnaryCall;
  allChats(argument: _chatProto_allChatRequest, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_chatProto_allChatResponse__Output>): grpc.ClientUnaryCall;
  allChats(argument: _chatProto_allChatRequest, metadata: grpc.Metadata, callback: grpc.requestCallback<_chatProto_allChatResponse__Output>): grpc.ClientUnaryCall;
  allChats(argument: _chatProto_allChatRequest, options: grpc.CallOptions, callback: grpc.requestCallback<_chatProto_allChatResponse__Output>): grpc.ClientUnaryCall;
  allChats(argument: _chatProto_allChatRequest, callback: grpc.requestCallback<_chatProto_allChatResponse__Output>): grpc.ClientUnaryCall;
  
}

export interface ChatAppHandlers extends grpc.UntypedServiceImplementation {
  allChats: grpc.handleUnaryCall<_chatProto_allChatRequest__Output, _chatProto_allChatResponse>;
  
}

export interface ChatAppDefinition extends grpc.ServiceDefinition {
  allChats: MethodDefinition<_chatProto_allChatRequest, _chatProto_allChatResponse, _chatProto_allChatRequest__Output, _chatProto_allChatResponse__Output>
}
