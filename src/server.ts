import path from 'path'
import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';


import type { ProtoGrpcType } from './Proto/ChatProto'

import type { ChatAppHandlers } from './Proto/chatProto/ChatApp'

const PROTO_FILE_DIR = './Proto/ChatProto.proto'
// Instantiating chatPackage
const packageDef = protoLoader.loadSync(path.resolve(__dirname, PROTO_FILE_DIR))
const grpcObj = (grpc.loadPackageDefinition(packageDef) as unknown) as ProtoGrpcType;
const chatPackage = grpcObj.chatProto


const PORT = 8083;


const main = () => {
    const server = getServer();

    server.bindAsync(`0.0.0.0:${PORT}`, grpc.ServerCredentials.createInsecure(), (err, port) => {
        if (err) {
            console.error(err);
            return
        }
        console.log(`Your server is running in port ${port}`)
        server.start()
    })

}


const getServer = () => {
    const server = new grpc.Server();

    server.addService(chatPackage.ChatApp.service, {
        "allChats": (req, res) => {
            console.log(req.request)
            res(null, { message: "Im from the server" })
        }
    } as ChatAppHandlers)
    return server
}

main()