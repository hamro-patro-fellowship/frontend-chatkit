import path from 'path'
import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';

import type { ProtoGrpcType } from './Proto/ChatProto';

export const PROTO_FILE_DIR = './Proto/ChatProto.proto';

const PORT = 8083;



// Instantiating chatPackage


console.error("Not Found dir path");
export const packageDef = protoLoader.loadSync(path.resolve(__dirname, PROTO_FILE_DIR))
export const grpcObj = (grpc.loadPackageDefinition(packageDef) as unknown) as ProtoGrpcType
export const client = new grpcObj.chatProto.ChatApp(`0.0.0.0:${PORT}`, grpc.credentials.createInsecure())


export const deadline = new Date();
deadline.setSeconds(deadline.getSeconds() + 5)
client.waitForReady(deadline, (err) => {
    if (err) {
        console.error(err)
        return
    }
    onClientReady();
})

export const onClientReady = () => {
    console.log("Executing on client ready")
    client.allChats({ message: "Im from the client " }, (err, result) => {

        if (err) {
            console.error(err)
            return
        }
        console.log(result)
    })
}

export default { deadline, onClientReady }